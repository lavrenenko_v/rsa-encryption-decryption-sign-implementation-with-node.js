const crypto = require ('crypto');
const {publicKey, privateKey} = require('./keys');


// encryption
const data = "Hello, world!"

const encryptedData = crypto.publicEncrypt({
    key:publicKey,
    padding:crypto.constants.RSA_PKCS1_OAEP_PADDING,
    oaepHash:"sha256"

}, Buffer.from(data));

console.log(`Encrypted data: ${encryptedData.toString("base64")}`);

// decryption
const decryptedData = crypto.privateDecrypt({
    key:privateKey,
    padding:crypto.constants.RSA_PKCS1_OAEP_PADDING,
    oaepHash:"sha256"
}, encryptedData);

console.log(`Decrypted data: ${decryptedData.toString()}`);
