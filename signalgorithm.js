const crypto = require('crypto');
const {publicKey, privateKey} = require('./keys');

const dataToBeSigned = "this need to be verified";

// Signing the data
const signature = crypto.sign("sha256", Buffer.from(dataToBeSigned), {
    key: privateKey,
    padding: crypto.constants.RSA_PKCS1_PSS_PADDING
})


console.log(`Signature: ${signature.toString("base64")}`);

// Verifying the signature
const isVerified = crypto.verify("sha256", Buffer.from(dataToBeSigned), {
    key:publicKey,
    padding:crypto.constants.RSA_PKCS1_PSS_PADDING
},signature);

console.log(`Is our signature valid?: ${isVerified?'Yes':'No'}`);

